import Vue from 'vue';
import App from './JsTreeApp.vue';

new Vue({
  el: '#app',
  render: h => h(App)
});

