import 'core-js';
import Vue from 'vue';
import BootstrapVue from 'bootstrap-vue';
import VueI18n from 'vue-i18n';

import FrameworkPlugin from 'framework/core/FrameworkPlugin.js';

import App from './SbAdmin.vue';


import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

// 플러그인 사용 
Vue.use(VueI18n)
Vue.use(BootstrapVue);
Vue.use(FrameworkPlugin);

const i18n  = new VueI18n({
  locale : 'ja',
  messages : {
    en : {}
  }
});



new Vue({
  i18n,
  el: '#app',
  render: h => h(App)
});

