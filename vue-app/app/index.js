import Vue from 'vue';
import App from './Index.vue';
import FrameworkPlugin from './framework/core/FrameworkPlugin.js';

// not working below
//import 'expose-loader?$!expose-loader?jQuery!jquery'
//import 'expose-loader?$!expose-loader?jQuery!jquery'


// 플러그인 사용 
Vue.use(FrameworkPlugin);

new Vue({
  el: '#app',
  render: h => h(App)
});
