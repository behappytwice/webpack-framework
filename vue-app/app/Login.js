import Vue from 'vue';
import App from './biz/login/Login.vue';
import 'expose-loader?$!expose-loader?jQuery!jquery'
import FrameworkPlugin from './framework/core/FrameworkPlugin.js';


// 플러그인 사용 
Vue.use(FrameworkPlugin);

new Vue({
  el: '#app',
  render: h => h(App)
});

