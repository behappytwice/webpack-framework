import 'core-js';
import Vue from 'vue';
import BootstrapVue from 'bootstrap-vue';
import VueI18n from 'vue-i18n';
import 'expose-loader?$!expose-loader?jQuery!jquery'
import FrameworkPlugin from 'framework/core/FrameworkPlugin.js';

import App from './Test.vue';

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.use(FrameworkPlugin);

new Vue({
    el: '#app',
    render: h => h(App)
  });
  