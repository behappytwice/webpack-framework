/**
 * 애플리케이션 전역에서 사용할 메소드나 디렉티브 등을 정의합니다. 
 */
export default  {
    install(Vue, options) {
        Vue.mixin( {
            mounted() {
                console.log("Plugin loaded.");
            },
            methods : { 
                testMethod : function() {
                    console.log("FrameworkPlugin.testMtehod() called.");
                }//: testMethod
            },// methods:
            data : function() {
                return {
                    appContext : ""  // ekp이면  /ekp라고 설정
                }
            }

        })// mixin
    }// install
};

