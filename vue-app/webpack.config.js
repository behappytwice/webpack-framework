var path = require('path');
var HtmlWebPackPlugin = require('html-webpack-plugin');
var CopyWebPackPlugin = require('copy-webpack-plugin');
var { CleanWebpackPlugin } = require('clean-webpack-plugin');
var VueLoaderPlugin = require('vue-loader/lib/plugin')
var MiniCssExtractPlugin = require('mini-css-extract-plugin');
var webpack = require('webpack');


module.exports = {
    mode:'development',  // development, production
    resolve : {
        alias : {
            framework : path.resolve(__dirname, 'app/framework')
        }
    },
    entry: {
        index : './app/index.js',
        login:  './app/login.js',
        jstree: './app/examples/vuejs/jstree/JsTreeApp.js',
        test : './app/test/test.js',
        sbadmin: './app/examples/vuejs/sbadmin/SbAdmin.js',
        main : './app/main.js'
        //vuemain : './src/vuemain.js'
    },
    output: {
      filename: '[name].js',
      chunkFilename : '[name].bundle.js',
      publicPath : "/vue-app/",
      //path: path.resolve(__dirname, 'dist')
      path: path.resolve(__dirname, '../src/main/webapp/vue-app')
    },
    // 중복된 것은 분리(split) 
    optimization: {
        splitChunks: {
            cacheGroups: {
                commons: {
                test: /[\\/]node_modules[\\/]/,
                name: 'vendors',
                chunks: 'all', // initial 초기 로드 되는 것만 청크에 추가, async 동적 로드된느 것만 청크에 추가, all 은 initial+ async
                },
            },
        },
    },
    module: {
        rules: [
            {
            // test에 로딩할 파일을 지정하고  use에 적용할 로더를 설정한다.
            test: /\.js$/,
            // nodes_modules 폴더는 패키지 폴더이므로 제외한다. 
            exclude: /node_modules/,
            use: {
                //  ES6에서 ES5로 변환할 때 바벨을 사용할수 
                loader:'babel-loader',
                options: {
                    presets: ['@babel/preset-env']
                }
            }
        },
        {  test : /\.vue$/, loader :'vue-loader'   },
        {
            resourceQuery: /blockType=i18n/,
            type: 'javascript/auto',
            loader: '@kazupon/vue-i18n-loader'
        },
        {
            test: /\.(png|jpe?g|gif)$/i,
            use: [
                {
                  loader: 'file-loader',
                  options : {
                      name : '[name].[ext]',
                      outputPath :'images'
                      //publicPath : 'assets'
                  }
                }
              ]
        },
        //{  test : /\.css$/,   use : { loader : 'ignore-loader'   }   }
        // css-loader, style-loader
        // https://www.zerocho.com/category/Webpack/post/58ac2d6f2e437800181c1657
        //{ test: /\.css$/, use: ['style-loader','css-loader'] }
        { test: /\.css$/, use : [
            MiniCssExtractPlugin.loader, // instead of style-loader
            'css-loader'
        ]}
        ] //:rules
    }, //: module
    plugins: [
      
        new CleanWebpackPlugin(),  // dist 폴더의 내용을 모두 삭제 
        new MiniCssExtractPlugin(), // css을 xxx.css 파일로 생성

        // https://choiyb2.tistory.com/96
        /*
        new HtmlWebPackPlugin({
            template:'./src/main/webapp-src/views/index.html',
            filename:'./index.html',
            chunks : ['index']
            //excludeChunks : [ 'main', 'vuemain' ]
        }),
        */
        /*
        new HtmlWebPackPlugin({
            template:'./src/index2.html',
            filename:'./index2.html',
            //excludeChunks : [ 'app', 'vuemain'],
            chunks : ['main']
        }),
        new HtmlWebPackPlugin({
            template:'./src/index3.html',
            filename:'./index3.html',
            //excludeChunks : [ 'app', 'main'],
            chunks : ['vuemain']
        }),
        */
        new VueLoaderPlugin(),
        // 파일 이동
        /*
        new CopyWebPackPlugin([
            { from : './src/assets',  to:'assets' },
            { from : './src/images',  to:'images' }
        ])
        */
 

       ] //: plugins
};